﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instance_PopUp : MonoBehaviour
{
    public GameObject particle;
    public float spawnTime;
    public float elapsedTime = 0f;
    public float ypos_inst;    //Son las posiciones maxima y minima en y donde podra aparecer el objeto.
    public float xpos_inst;             //Es la posicion inicial en x donde se instanciara el objeto.
    public float instanceTime;     //Marca el inicio de instanciacion a partir de un valor de segundos

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameControler.instance.gameover == false && GameControler.instance.totaltimeSeconds > instanceTime )
        {


            if (elapsedTime < spawnTime)
            {
                elapsedTime += Time.deltaTime;
            }
            else
            {
                Instantiate(particle, new Vector3(xpos_inst, ypos_inst, 0), Quaternion.identity);
                elapsedTime = 0;
            }

        }
    }
}
