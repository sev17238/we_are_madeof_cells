﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour {

    public float scrollingSpeed = 5f;
    public float position;
    public float startS;     //Marca el inicio de instanciacion a partir de un valor de segundos
    public float stopS;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameControler.instance.totaltimeSeconds > startS && GameControler.instance.totaltimeSeconds < stopS)
        {
            transform.Translate(new Vector3(-1, 0, 0) * scrollingSpeed * Time.deltaTime);
            if (transform.position.x < -position)
            {
                transform.position = new Vector3(position, transform.position.y, transform.position.z);
            }

        }
    }
}
