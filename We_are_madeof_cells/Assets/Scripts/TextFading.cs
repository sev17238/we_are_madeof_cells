﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFading : MonoBehaviour
{
    //SpriteRenderer sr;
    float alpha = 0f;
    public float startReverting;
    public float startFading;

    Text textt;

    // Start is called before the first frame update
    void Start()
    {
        //sr = GetComponent<SpriteRenderer>();
        textt = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

        if (GameControler.instance.totaltimeSeconds > startReverting && GameControler.instance.totaltimeSeconds < startFading)
        {
            if (textt.color.a < 1)
            {
                alpha = alpha + 0.01f;
                textt.color = new Color(textt.color.r, textt.color.g, textt.color.b, alpha);
            }
        }
       
        if (GameControler.instance.totaltimeSeconds >= startFading)
        {
            if (textt.color.a >= 0)
            {
                alpha = alpha - 0.01f;
                textt.color = new Color(textt.color.r, textt.color.g, textt.color.b, alpha);
            }

        }

        if (GameControler.instance.totaltimeSeconds >= startFading + 3)
        {
            Destroy(gameObject);
        }


    }
}
