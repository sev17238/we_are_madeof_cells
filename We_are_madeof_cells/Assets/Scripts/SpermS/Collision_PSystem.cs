﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision_PSystem : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "particle" && GameControler.instance.gameover == false)
        {
            //GameControler.instance.score++;
            collision.SendMessage("EnemyKnockBack", transform.position.x); //se llama al metodo EnemyKnockBack de Player

        }

    }
}
