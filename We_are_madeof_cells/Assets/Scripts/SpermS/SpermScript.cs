﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpermScript : MonoBehaviour {

    Rigidbody2D rb2d; //referencia al cuerpo rigido 2d
    public float speed = 5f; //Variable para la velocidad horizontal que tendra la celula
    public float MaxSpeed = 6f;

    public float impulseForce = 15f;
    public bool impulse = false;

    public AudioSource audio;
    public Animator anim;
    public AudioClip Speed_impulse;
    public AudioClip audioDamage;
    public AudioClip HCapsule;
    public AudioClip Death;
    //[SerializeField]
    //GameObject Particules_test; 

    public int health = 3; //variable para la vida del jugador
    private bool death = false; //para activar animacion de death
    private bool movement = true;

    public float stopReposition = 180; //variable para detener el reposicionamiento del jugador al final del juego
    public float stageComplete; //Tiempo para pasar a stagecomplete


    //Limitar la posicion en x del jugador para que no se salga
    //con alfa se puede hacer desaparecer las imagenes
    //con otra variable igual al tiempo se puede disminuir el spawntime
    void Start() {
        rb2d = GetComponent<Rigidbody2D>();
        audio = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {

        //float move = Input.GetAxis("Horizontal"); //se toma la entrada en el eje horizontal para PC
        //if (movement == false) move = 0;
        movement = true;
        //if (move != 0)
        if (movement == true)
        {
            
            if (GameControler.instance.gameover == false && GameControler.instance.endGame == false)
            {
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    
                    rb2d.transform.Translate(Vector2.up * speed * Time.deltaTime); //el cuerpo rigido se traslada

                }
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    rb2d.transform.Translate(Vector2.down * speed * Time.deltaTime); //el cuerpo rigido se traslada
                }
                else if (Input.GetKey(KeyCode.LeftArrow) && rb2d.position.x > -6.50f) //Limite para que el jugador no pueda salirse de pantalla.
                {
                    rb2d.transform.Translate(Vector2.left * speed * Time.deltaTime); //el cuerpo rigido se traslada
                }
                else if (Input.GetKey(KeyCode.RightArrow) && rb2d.position.x < 5.50f)
                {
                    rb2d.transform.Translate(Vector2.right * speed * Time.deltaTime); //el cuerpo rigido se traslada
                }
                else if (Input.GetKey(KeyCode.X) && rb2d.position.x <= 5.50f)
                {
                    //Invoke("SpeedImpulseSound", 1); //No suena bien
                    anim.SetTrigger("Speed_impulse");
                    rb2d.transform.Translate(Vector2.right * speed*2.5f * Time.deltaTime);
                    //impulse = true;
                    //rb2d.velocity = new Vector2(rb2d.velocity.x, 0);
                    //rb2d.AddForce(Vector2.right *impulseForce, ForceMode2D.Impulse);                //Este impulso ocurre en un evento de animacion
                    //rb2d.transform.Translate(Vector2.right * speed * 2 * Time.deltaTime); //Impulso de velocidad
                }

            }
            if (GameControler.instance.endGame == true)
            {
                if(GameControler.instance.totaltimeSeconds < stopReposition) {
                    if(rb2d.position.y > -0.2f)
                    {
                        if(rb2d.position.y > -0.2f) { 
                            rb2d.transform.Translate(Vector2.down * speed * Time.deltaTime); //el cuerpo rigido se traslada
                        }
                    }
                    else
                    {
                        if (rb2d.position.y < -0.2f)
                        {
                            rb2d.transform.Translate(Vector2.up * speed * Time.deltaTime); //el cuerpo rigido se traslada
                        }
                    }

                    if (rb2d.position.x > -6.40f)
                    {
                        rb2d.transform.Translate(Vector2.left * speed * Time.deltaTime); //el cuerpo rigido se traslada
                    }
                }
                if (GameControler.instance.totaltimeSeconds > stageComplete-5f) {
                    if (rb2d.position.x < 2f)
                    {
                        rb2d.transform.Translate(Vector2.right * speed/3.5f * Time.deltaTime); //el cuerpo rigido se traslada
                    }
                }
                if (GameControler.instance.totaltimeSeconds > stageComplete)
                {
                    Invoke("StageComplete", 1);
                }

            }
            /*else
            { //si gameover es verdadero
                Invoke("GameOver", 2);
            } /////////////////////////////////PUEDE HABER PROBLEMA ACA/////////////////////////////////////////////*/
        }
    }

    /*private void FixedUpdate()
    {
        if(impulse == true)
        {
           // SpeedImpulse();
            impulse = false;            
        }
 
    }
    */

    private void OnTriggerEnter2D(Collider2D collision) //cuando hay una colision 
    {
        if (GameControler.instance.gameover == false) {
            if (collision.gameObject.tag == "whitecell" || collision.gameObject.tag == "column" ||
                collision.gameObject.tag == "ceiling")
            {
                Sperm_Death();
            }
            
            if (collision.gameObject.tag == "HCapsule")
            {
                PlayAudioClipLowVol(HCapsule);
                if (health < 3) {
                    health = health + 1;
                }
                Debug.Log("Capsule");

            }
            if (collision.gameObject.tag == "particle")
            {
                Debug.Log("Particle");
                EnemyKnockBack();
            }
        }
    }

    public void EnemyKnockBack()
    {
        if (health >= 2)
        {
            PlayAudioClipMidVol(audioDamage);
            health -= 1;
            movement = false;
            Invoke("EnableMovement", 1); //se vuelve a habilitar el movimiento luego de 1 segundo
            anim.SetTrigger("Damage");
        }
        else
        {
            Sperm_Death();
        }
    }

    void EnableMovement()
    {
        movement = true;
    }

    void Sperm_Death()
    {
        GameControler.instance.gameover = true;

        PlayAudioClipMidVol(Death);
        health = 0;
        anim.SetTrigger("Death");
        anim.SetBool("Death2", true);
        movement = false;
        death = true;

        Invoke("GameOver", 3);
    }

    void SpeedImpulse()
    {
        PlayAudioClipHighVol(Speed_impulse);
        /*float limitedSpeed = Mathf.Clamp(rb2d.velocity.x, -MaxSpeed, MaxSpeed);
        rb2d.velocity = new Vector2(limitedSpeed, 0);
        rb2d.AddForce(Vector2.right *impulseForce, ForceMode2D.Impulse);  */
        if(rb2d.position.x <= 5.50f) { 
            rb2d.transform.Translate(Vector2.right * impulseForce * 10 * Time.deltaTime);
        }
    }

    void SpeedImpulseSound()
    {
        PlayAudioClipHighVol(Speed_impulse);
    }
        /**
         * Metodo para llamar a la escena de gameover
         * 
         */
    void GameOver()
    {
        GameControler.instance.ChangeScene("GameOver");
    }

    void StageComplete()
    {
        GameControler.instance.ChangeScene("StageComplete-Sperm");
    }

    /* Metodo para audios con poco volumen. Se utiliza 1 para incrementar su volumen.
     */
    void PlayAudioClipMidVol(AudioClip audioclip)
    {
        audio.clip = audioclip;
        audio.loop = false;
        audio.volume = 0.5f;
        audio.Play();
    }

    /* Metodo para audios con medio volumen. Se utiliza un 0.50.
     */
    void PlayAudioClipLowVol(AudioClip audioclip)
    {
        audio.clip = audioclip;
        audio.loop = false;
        audio.volume = 1;
        audio.Play();
    }

    /* Metodo para audios con mucho volumen. Se utiliza un 0.25 para disminuir su volumen.
     */
    void PlayAudioClipHighVol(AudioClip audioclip)
    {
        audio.clip = audioclip;
        audio.loop = false;
        audio.volume = 0.25f;
        audio.Play();
    }



}
