﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls_fading : MonoBehaviour
{
    SpriteRenderer sr;
    float alpha = 0f;
    public float startReverting;
    public float startFading;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if (GameControler.instance.totaltimeSeconds > startReverting && GameControler.instance.totaltimeSeconds < startFading)
        {
            if (sr.color.a < 1)
            {
                alpha = alpha + 0.01f;
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha);
            }
        }

        if (GameControler.instance.totaltimeSeconds >= startFading)
        {
            if (sr.color.a >= 0)
            {
                alpha = alpha - 0.01f;
                sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha);
            }

        }
        
        if (GameControler.instance.totaltimeSeconds >= startFading+3) { 
            Destroy(gameObject);
        }            
       

    }
}
