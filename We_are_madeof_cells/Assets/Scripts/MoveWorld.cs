﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWorld : MonoBehaviour
{
    public float limitXposition, limitXposition2;
    public float destroyXposition;
    public float startS;
    public float startS2;
    public bool variousMovments;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(variousMovments == true)
        {
            if (GameControler.instance.totaltimeSeconds > startS && GameControler.instance.totaltimeSeconds < startS+5)
            {
                if (transform.position.x > limitXposition)
                {

                    transform.Translate(Vector3.left * 10f * Time.deltaTime);
                }
            }
            if (GameControler.instance.totaltimeSeconds > startS2)
            {
                if (transform.position.x > limitXposition2)
                {

                    transform.Translate(Vector3.left * 10f * Time.deltaTime);
                }
            }

            if (transform.position.x < destroyXposition)
            {
                Destroy(gameObject);
            }
        }
        else { 
            if (GameControler.instance.totaltimeSeconds > startS)
            {            
                if (transform.position.x > limitXposition2) { 
            
                    transform.Translate(Vector3.left * 10f * Time.deltaTime);
                }
            }

            if (transform.position.x < destroyXposition)
            {            
                Destroy(gameObject);
            }
        }
    }
}
