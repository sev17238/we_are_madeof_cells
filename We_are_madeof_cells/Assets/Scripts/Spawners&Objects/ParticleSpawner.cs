﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    public GameObject particle;
    public float spawnTime;
    public float elapsedTime = 0f;
    public float ypos_min, ypos_max;    //Son las posiciones maxima y minima en y donde podra aparecer el objeto.
    public float xpos_instantiate;             //Es la posicion inicial en x donde se instanciara el objeto.
    public float startS;     //Marca el inicio de instanciacion a partir de un valor de segundos
    public float stopS;     
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (GameControler.instance.gameover == false && GameControler.instance.totaltimeSeconds > startS &&
            GameControler.instance.totaltimeSeconds < stopS)
        {
            //con otra variable que tome el valor del tiempo
            /*if (GameControler.instance.timeS == 17.0 && GameControler.instance.timeM == 0) 
            {
                spawnTime = spawnTime - 3;
            }
            if (GameControler.instance.timeS == 17.0 && GameControler.instance.timeM == 1)
            {
                spawnTime = spawnTime - 1;
            }*/

            if (elapsedTime < spawnTime)
            {
                elapsedTime += Time.deltaTime;
            }
            else
            {
                float random = Random.Range(ypos_min, ypos_max);
                //float random = Random.Range(-4.5f, -3.5f);
                Instantiate(particle, new Vector3(xpos_instantiate, random, 0), Quaternion.identity);
                elapsedTime = 0;
            }
        }


    }

    
}
