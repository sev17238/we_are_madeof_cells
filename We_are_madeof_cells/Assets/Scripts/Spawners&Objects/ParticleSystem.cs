﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystem : MonoBehaviour
{
    public float scrollingSpeed_min, ScrollingSpeed_max;
    // Start is called before the first frame update
    private ParticleSystem particles;


    void Start()
    {
        Quaternion target = Quaternion.Euler(-90.0f, 0, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, target, 1);

    }

    // Update is called once per frame
    void Update()
    {
        //if (GameControler.instance.gameover == false)
        //{        

        float randomSpeed = Random.Range(scrollingSpeed_min, ScrollingSpeed_max);
        transform.Translate(Vector3.left * randomSpeed * Time.deltaTime);
        if (transform.position.x < -10)
        {
            DestroyObj();
        }
        //}
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" && GameControler.instance.gameover == false)
        {
            //GameControler.instance.score++;
            collision.SendMessage("EnemyKnockBack", transform.position.x); //se llama al metodo EnemyKnockBack de Player
            Debug.Log("mierda");
            DestroyObj();
        }

    }

    private void DestroyObj()
    {
        Destroy(gameObject);
    }
}
