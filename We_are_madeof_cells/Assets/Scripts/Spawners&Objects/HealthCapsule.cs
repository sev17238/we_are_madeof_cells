﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCapsule : MonoBehaviour {

    public float scrollingSpeed_min, ScrollingSpeed_max;
    // Start is called before the first frame update


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        float randomSpeed = Random.Range(scrollingSpeed_min, ScrollingSpeed_max);
        transform.Translate(Vector3.left * randomSpeed * Time.deltaTime);
        if (transform.position.x < -10)
        {
            DestroyObj();
        }
 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            //GameControler.instance.score++;
            Destroy(gameObject);
        }

    }

    private void DestroyObj()
    {
        Destroy(gameObject);
    }
}
