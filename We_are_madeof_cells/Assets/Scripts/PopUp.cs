﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUp : MonoBehaviour
{
    public float scrollingSpeed;
    public float limitYposition;
    public float ShowPopup;
    public float HidePopup;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameControler.instance.totaltimeSeconds > ShowPopup && GameControler.instance.totaltimeSeconds < HidePopup)
        {
            if (transform.position.y < limitYposition)
            {
                transform.Translate(Vector3.up * scrollingSpeed * Time.deltaTime);
                
            }
        }

        if (GameControler.instance.totaltimeSeconds >= HidePopup)
        {
            transform.Translate(Vector3.down * scrollingSpeed * Time.deltaTime);
        }

        if (GameControler.instance.totaltimeSeconds >= HidePopup + 2)
        {
            Destroy(gameObject);
        }

    }
}
