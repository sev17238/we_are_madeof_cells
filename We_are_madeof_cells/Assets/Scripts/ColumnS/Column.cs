﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column : MonoBehaviour {

    //public float scrollingSpeed = 5f;
    public float scrollingSpeed_min, ScrollingSpeed_max;

    void Start () {
		
	}
	
	void Update () {
        //if (GameControler.instance.gameover == false)
        //{
            float randomSpeed = Random.Range(scrollingSpeed_min, ScrollingSpeed_max);
            transform.Translate(Vector3.left * randomSpeed * Time.deltaTime);
            if (transform.position.x < -12)
            {
                Destroy(gameObject);
            }
        //}
    }

}
