﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmashingColumn : MonoBehaviour
{
    public float scrollingSpeed_min, ScrollingSpeed_max;
    public float scrollingYSpeed_min, ScrollingYSpeed_max;
    public float limitYposition;
    public bool upside;      //true si es la columna de arriba o false si es la columna de abajo
    //public int up0rdownV;     //se multiplicara por 1 o -1 la velocidad de la columna dependiendo si es la de abajo 
                              //o la de arriba.                                                                

    void Start()
    {
       /*if(upside == true) { 
            Quaternion target = Quaternion.Euler(0, 0, 180f);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, 1);
        }*/
    }

    void Update()
    {
        //if (GameControler.instance.gameover == false)
        //{
        if(upside == true)
        {
            if (transform.position.y > limitYposition)
            {
                float randomYSpeed = Random.Range(scrollingYSpeed_min, ScrollingYSpeed_max);
                transform.Translate(Vector3.down * randomYSpeed * Time.deltaTime);
            }            
        }
        else
        {
            if (transform.position.y < limitYposition)
            {
                float randomYSpeed = Random.Range(scrollingYSpeed_min, ScrollingYSpeed_max);
                transform.Translate(Vector3.up * randomYSpeed * Time.deltaTime);
            }
        }

        float randomSpeed = Random.Range(scrollingSpeed_min, ScrollingSpeed_max);
        transform.Translate(Vector3.left * randomSpeed * Time.deltaTime);
        if (transform.position.x < -12)
        {
            Destroy(gameObject);
        }
        //}
    }
}
