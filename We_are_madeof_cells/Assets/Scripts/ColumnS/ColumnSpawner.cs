﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnSpawner : MonoBehaviour {

    public GameObject column;
    public float spawnTime;
    public float elapsedTime = 0f;
    public float ypos_min, ypos_max;

    public float startS;     //Marca el inicio de instanciacion a partir de un valor de segundos
    public float stopS;     //Marca un alto para la instanciacion en segundos

                             // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (GameControler.instance.gameover == false && GameControler.instance.timeS > startS &&
            GameControler.instance.totaltimeSeconds < stopS)
        {
            /*if (GameControler.instance.timeS == 17.0 && GameControler.instance.timeM == 0)
            {
                spawnTime = spawnTime - 4;
            }
            if (GameControler.instance.timeS == 17.0 && GameControler.instance.timeM == 1)
            {
                spawnTime = spawnTime - 1;
            }*/

            if (elapsedTime < spawnTime)
            {
                elapsedTime += Time.deltaTime;
            }
            else
            {
                float random = Random.Range(ypos_min, ypos_max);
                //float random = Random.Range(-4.5f, -3.5f);
                Instantiate(column, new Vector3(12, random, 0), Quaternion.identity);
                elapsedTime = 0;
            }

            
        }
	}
}
