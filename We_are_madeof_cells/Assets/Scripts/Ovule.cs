﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ovule : MonoBehaviour
{
    public float limitXposition;
    public float startS;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameControler.instance.totaltimeSeconds > startS) { 
            if (transform.position.x > limitXposition)
            {
                transform.Translate(Vector3.left * 5f * Time.deltaTime);
            }
        }
    }
}
