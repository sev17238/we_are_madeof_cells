﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControler : MonoBehaviour {

    public bool gameover = false; //variable para terminar el juego
    public bool endGame = false;
    public int Score = 0; //el punteo
    public float timeS = 0;
    public float timeM = 0;
    public float totaltimeSeconds = 0;
    private float timeSR = 0;
    private float timeMR = 3;

    public Text textScore;
    public Text textTime;


    public static GameControler instance; //variable que sirve para poder accesar a variables Ej. Score

    // Use this for initialization
    void Start() {
        instance = this;
       
    }

    // Update is called once per frame
    void Update() {
        if(gameover == false) { 
            
            //Cuenta ascendente del tiempo
            if (timeS <= 9) {
                //textTime.text = "0" + timeM.ToString("n0") + ":" + "0" + timeS.ToString("n0");
            } else {
                //textTime.text = "0" + timeM.ToString("n0") + ":" + timeS.ToString("n0");
            }
            if (timeS <= 59)
            {
                timeS = timeS + Time.deltaTime;
                totaltimeSeconds =+ totaltimeSeconds + Time.deltaTime;
            } else {
                timeS = 0;
                timeM = timeM + 1;
            }

            //Cuenta descendente del tiempo
            if (timeSR <= 9)
            {
                textTime.text = "0" + timeMR.ToString("n0") + ":" + "0" + timeSR.ToString("n0");
            }else{
                textTime.text = "0" + timeMR.ToString("n0") + ":" + timeSR.ToString("n0");
            }
            if (timeSR > 0)
            {
                timeSR = timeSR - Time.deltaTime;
            }
            if (timeSR <= 0 && timeMR > 0)
            {
                timeSR = 59;
                timeMR = timeMR - 1;
            }
            else if (timeMR == 0 && timeSR == 0)
            {
                endGame = true;
            }
        }

        if(totaltimeSeconds > 177)
        {
            endGame = true;
        }

    }
    public void ChangeScene(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }

    /*public void GameOver_Scene()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Sperm_level")
        {
            SceneManager.LoadScene(scenename);
        }

    }*/

    public void QuitGame()
    {
        Application.Quit();
    }
}
