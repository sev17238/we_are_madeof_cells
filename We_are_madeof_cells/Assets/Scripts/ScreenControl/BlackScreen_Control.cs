﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackScreen_Control : MonoBehaviour
{
    SpriteRenderer sr;
    float alpha1 = 1f;
    float alpha2 = 0f;
    public float startFading;
    public float startReverting;
    public bool firstFading;
    public float FadeRevert_Fac; //0.1f velocidad rapida, 0.01f velocidad media, 0.001f velocidad lenta
    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(firstFading == true) { 
            if(GameControler.instance.totaltimeSeconds > startFading) { 
                if (sr.color.a >= 0)
                {
                    alpha1 = alpha1 - FadeRevert_Fac;
                    sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha1);
                }                        
            }
            //else
            //{ 
                if (GameControler.instance.totaltimeSeconds >= startFading+5)
                {
                    Destroy(gameObject);
                }
            //}

        }else
        {

            if (GameControler.instance.totaltimeSeconds > startReverting)
            {
                if (sr.color.a <= 1)
                {
                    alpha2 = alpha2 + FadeRevert_Fac;
                    sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, alpha2);
                }
            }
            //else
            //{
                if (GameControler.instance.totaltimeSeconds >= startReverting + 5)
                {
                    Destroy(gameObject);
                }
            //}
        }

    }
}
