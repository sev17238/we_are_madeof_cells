﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScallingImage : MonoBehaviour
{
    SpriteRenderer sr;
    public float startScaling;
    public float scaling_fac; //0.001, 0.01, 0.1
    Vector2 newScale;
    

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GameControler.instance.totaltimeSeconds > startScaling)
        {
            Vector2 scale = transform.localScale;
            Vector2 spritesize = sr.sprite.bounds.size;
            if (transform.localScale.x >1.04f && transform.localScale.y > 1.00f) {                
                newScale = new Vector2(transform.localScale.x - scaling_fac, transform.localScale.y - scaling_fac);
                spritesize = scale;
            }
           
        }
    }
}
